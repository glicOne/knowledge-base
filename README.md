# Knowledge base
pdf,txt,djvu, various useful books

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Vadim Romaniuk** - *Initial work* - [glicOne](https://gitlab.com/glicOne)

See also the list of [contributors](https://gitlab.com/glicOne/knowledge-base/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
