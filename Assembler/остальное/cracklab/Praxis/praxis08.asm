      .386
      .model flat, stdcall
      option casemap :none   ; case sensitive
;#########################################################################
      include \masm32\include\windows.inc
      include \masm32\include\user32.inc
      include \masm32\include\kernel32.inc

      includelib \masm32\lib\user32.lib
      includelib \masm32\lib\kernel32.lib
;#########################################################################
    .data
	MsgBoxCaption db "It's the first your debuging for Win32",0
	MsgBoxText    db "Assembler language for Windows is a fable!",0
;#########################################################################
    .code

start:

    mov    EAX, offset MsgBoxCaption
    mov    EBX, 00403021             ; ����� 1-� ������. �������� ������ ���� ����������������� (�������� ����� h).

    push   EAX
    push   EBX

    push   0
    push   EAX
    push   EBX
    push   0
    call   MessageBox
; ------------------------
    pop    EAX                       ; ������ ������. �� ����� �������� ����������� � �������� �������.
    pop    EBX

    push   0
    push   EAX
    push   EBX
    push   0
    call   MessageBox
; ------------------------
    pop    EBX                       ; ������ 3. � ����� ������ ��� ��������, ������� ��� �����.
    pop    EAX

    push   0
    push   EAX
    push   EBX
    push   0
    call   MessageBox
; ------------------------
    push   0
    call   ExitProcess
	
end start


