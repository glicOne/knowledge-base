      .386
      .model flat, stdcall
      option casemap :none   ; case sensitive
;#########################################################################
      include \masm32\include\windows.inc
      include \masm32\include\user32.inc
      include \masm32\include\kernel32.inc

      includelib \masm32\lib\user32.lib
      includelib \masm32\lib\kernel32.lib
;#########################################################################
.data
  MsgCaptionNO   db "������ ������������!",0
  MsgTextNO      db "��������� ��������� ������ �����������",0
  MsgCaptionYES  db "������������ ���������:",0
	
  Crypt_String   byte 100h dup (00) ; ����� �� dup - ����������, ������ - ��� ���������
;#########################################################################
    .code

start:
	call   Main        ; ����� ������� �������
	push   0           ; ������ �������� ��� ��������� ������
	call   ExitProcess ; ����� API-������� ������
;=========================================================================
Main proc                     ; ������ ������� �������
        call   Take_Arguments ; ������ � EAX ��������� �� ���� ��������� ������, ���� ��� - EAX=0.

        test   eax,eax        ; ��������� EAX �� ����
        jnz    Next1
        call   Message
        ret

Next1: 	call   Crypt
	call   Message
	ret
Main endp                     ; ����� ������� �������
;=========================================================================

Take_Arguments proc           ; ������ ������� ��������� ��������� ��������� ������
; ����  - ������.
; ����� - EAX= ��������� �� ��������� ��������� ������ ��� 0.

	call   GetCommandLine        
	mov    ECX,512d
        add    ECX,EAX               

    unquote:	
	inc    EAX                   
        cmp    EAX,ECX
        jz     NO_Arg
        cmp    byte ptr[EAX],22h  
    jnz unquote                   

    Arg_search:
	inc    EAX                  
        cmp    byte ptr[EAX],0   
        jz     NO_Arg            
                                 
        cmp    byte ptr[EAX],20h
    jz  Arg_search

        ret

NO_Arg:
	xor eax,eax
	ret    

Take_Arguments endp            ; ����� ������� ��������� ��������� ��������� ������
;-------------------------------------------------------------------------

Crypt proc                     ; ������ ������� ����������
; ����  - � EAX ������ ���� ��������� �� ������, 
;         ��������������� ���� (��� � ���� �������� �������).
; ����� - � EAX ����� ��������� �� ����������� ������.
; ������������ ���������� ���������� Crypt_String.

	push   EBX
	push   ECX
	push   EDX

	mov    EBX, offset Crypt_String
	mov    ECX,00001010b

   Crypt_Loop:
	movzx  EDX, byte ptr [EAX]
	test   EDX,EDX
	jz     Fin_Crypt
        xor    DL,CL
        mov    byte ptr [EBX],DL
        inc    EAX
        inc    EBX
        xor    CL, 00000101b
   jmp Crypt_Loop

Fin_Crypt:
        pop    EDX
        pop    ECX
        pop    EBX

	mov    EAX, offset Crypt_String
	ret

Crypt endp                     ; ����� ������� ����������
;-------------------------------------------------------------------------

Message proc                   ; ������ ������� ������ ���������
; ����  - EAX=0 ��� ��������� �� ������ ������, ��������������� ����.
; ����� - ������.
; ������������ ���������� ����������: 
; MsgCaptionYES, Crypt_String, MsgCaptionNO, MsgTextNO.

        test   EAX,EAX
        jz     No_Arguments

       	push   0                     
	push   offset MsgCaptionYES ; offset - ���������
	push   EAX                  
	push   0                    
	call   MessageBox           
        ret

No_Arguments:
   
        push   0
	push   offset MsgCaptionNO  ; ����� ���������
	push   offset MsgTextNO     ; ����� ������ ���������
	push   0                    ; ��� ������������� ����
	call   MessageBox           ; ����� API-������� ������ ��������� �� �����
	ret

Message endp                   ; ����� ������� ������ ���������
;-------------------------------------------------------------------------

end start
