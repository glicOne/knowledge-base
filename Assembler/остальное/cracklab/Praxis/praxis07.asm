      .386
      .model flat, stdcall
      option casemap :none   ; case sensitive
;#########################################################################
      include \masm32\include\windows.inc
      include \masm32\include\user32.inc
      include \masm32\include\kernel32.inc
      includelib \masm32\lib\user32.lib
      includelib \masm32\lib\kernel32.lib
;#########################################################################
    .data
	MsgBoxCaption  db "It's the first your command line for Win32",0
	MsgBoxText     db "��������� ��������� ������ �����������",0
;#########################################################################
    .code

start:
	call   GetCommandLine ; API. ���������� � EAX ����� ��������� ������
	mov    ECX,512d       ; � ECX ��������� �������� 200h
	add    ECX,EAX        ; ECX = 512d + ����� ��������� ������ 
	                      ; � ECX �� �������� ������� ����� ��������� ��������� ������
	                      ; � EAX ��������� �� � ������

   unquote:	                 ; ����� ����� ���������� ����������� �������
	inc    EAX               ; ��������� EAX �� 1 (������� ����� ���.������+1)
	cmp    EAX,ECX           ; ������� ������� ����� � ������� �������
	jz     NO                ; ���� ���� ����� - ����� �� ����� �� ����� NO (��������� �����)
	cmp    byte ptr[EAX],22h ; ������� ���������� ������, ��������� � EAX, � �������� (22h)
   jnz unquote                   ; ���� �� ������� (ZF ����� ��������), ����� ���� �� unquote

   Arg_search:                   ; ����� ����� ���������� ��������� ��������� ������
	inc    EAX               ; ���� ������� �������, ������, ��������� EAX �� 1
	cmp    byte ptr[EAX],0   ; ���� ��� ��� ���� 00, cmp ������� ���� ����
	jz     NO                ; ZF=1 - ������� ���������, ������, ����. ZF = 0 - ������ ���
	cmp    byte ptr[EAX],20h ; ������� ����, ��������� EAX, � �������� ������� (20h)
   jz  Arg_search                ; ���� ��� ��� ������ - ���� �� ������ �����,
                                 ; ���� ���, ������ �������� ������


  push   0                    ; ��� �������� ��������, � ���� �������� ��� API-�������
  push   offset MsgBoxCaption ; Offset - ���������. MASM ��������� ����� ����������
  push   EAX                  ; � ���� ���������� EAX. A���� ��������� - �������� ��� API
  push   0                    ; � ���� �������� ������������� ���� (��� ���)
  call   MessageBox           ; ����� API-������� ������ ��������� �� �����

  push   0                    ; ������ �������� ��� ��������� ������
  call   ExitProcess          ; ����� API-������� ������

NO:
  push   0                    ; �������� "MB_OK"
  push   offset MsgBoxCaption ; �������� "����� ���������"
  push   offset MsgBoxText    ; �������� "����� ������ ���������"
  push   0                    ; �������� "������������ ����"
  call   MessageBox           ; ����� API-������� ������ ��������� �� �����

  push   0                    ; ������ �������� ��� ��������� ������
  call   ExitProcess          ; ����� API-������� ������

end start