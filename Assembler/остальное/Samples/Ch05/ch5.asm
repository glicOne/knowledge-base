<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.6-c128 79.159124, 2016/03/18-14:01:55        ">
 <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about=""
    xmlns:photoshop="http://ns.adobe.com/photoshop/1.0/"
    xmlns:stCamera="http://ns.adobe.com/photoshop/1.0/camera-profile">
   <photoshop:CameraProfiles>
    <rdf:Seq>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="248"
       stCamera:Lens="135mm F1.8 DG HSM | Art 017"
       stCamera:LensInfo="135/1 135/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon"
       stCamera:LensPrettyName="SIGMA 135mm F1.8 DG HSM A017"
       stCamera:ProfileName="Adobe (SIGMA 135mm F1.8 DG HSM A017, Canon)"
       stCamera:FocalLength="135"
       stCamera:FocusDistance="10000"
       stCamera:ApertureValue="1.695994">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:ResidualMeanError="0"
        stCamera:ResidualStandardDeviation="0"
        stCamera:RadialDistortParam1="0.294859"
        stCamera:RadialDistortParam2="-2.213616"
        stCamera:RadialDistortParam3="-1.292747">
       <stCamera:ChromaticRedGreenModel
        stCamera:ResidualMeanError="0"
        stCamera:ResidualStandardDeviation="0"
        stCamera:ScaleFactor="0.999982"
        stCamera:RadialDistortParam1="0.007531"
        stCamera:RadialDistortParam2="-0.109655"
        stCamera:RadialDistortParam3="1.675408"/>
       <stCamera:ChromaticGreenModel
        stCamera:ResidualMeanError="0"
        stCamera:ResidualStandardDeviation="0"
        stCamera:RadialDistortParam1="0.294859"
        stCamera:RadialDistortParam2="-2.213616"
        stCamera:RadialDistortParam3="-1.292747"/>
       <stCamera:ChromaticBlueGreenModel
        stCamera:ResidualMeanError="0"
        stCamera:ResidualStandardDeviation="0"
        stCamera:ScaleFactor="1.000189"
        stCamera:RadialDistortParam1="-0.0131"
        stCamera:RadialDistortParam2="0.365167"
        stCamera:RadialDistortParam3="-7.07005"/>
       <stCamera:VignetteModel>
        <rdf:Description
         stCamera:ResidualMeanError="0.01511"
         stCamera:VignetteModelParam1="-55.114581"
         stCamera:VignetteModelParam2="2806.282106"
         stCamera:VignetteModelParam3="-59662.448848">
        <stCamera:VignetteModelPiecewiseParam>
         <rdf:Seq>
          <rdf:li>0.000000, 1.000000</rdf:li>
          <rdf:li>0.012283, 0.975601</rdf:li>
          <rdf:li>0.042425, 0.882534</rdf:li>
          <rdf:li>0.103764, 0.664503</rdf:li>
          <rdf:li>0.136726, 0.533510</rdf:li>
          <rdf:li>0.161198, 0.424530</rdf:li>
         </rdf:Seq>
        </stCamera:VignetteModelPiecewiseParam>
        </rdf:Description>
       </stCamera:VignetteModel>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="248"
       stCamera:Lens="135mm F1.8 DG HSM | Art 017"
       stCamera:LensInfo="135/1 135/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon"
       stCamera:LensPrettyName="SIGMA 135mm F1.8 DG HSM A017"
       stCamera:ProfileName="Adobe (SIGMA 135mm F1.8 DG HSM A017, Canon)"
    