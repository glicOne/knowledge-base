<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.3-c007 1.136881, 2010/06/10-18:11:35        ">
 <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about=""
    xmlns:photoshop="http://ns.adobe.com/photoshop/1.0/"
    xmlns:stCamera="http://ns.adobe.com/photoshop/1.0/camera-profile">
   <photoshop:CameraProfiles>
    <rdf:Seq>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="173"
       stCamera:Lens="150mm"
       stCamera:LensInfo="150/1 150/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon"
       stCamera:LensPrettyName="SIGMA APO MACRO 150mm F2.8 EX DG HSM"
       stCamera:ProfileName="Adobe (SIGMA APO MACRO 150mm F2.8 EX DG HSM, Canon)"
       stCamera:SensorFormatFactor="1"
       stCamera:FocalLength="150"
       stCamera:FocusDistance="999999995904"
       stCamera:ApertureValue="4">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:ResidualMeanError="0"
        stCamera:ResidualStandardDeviation="0"
        stCamera:RadialDistortParam1="0.140405"
        stCamera:RadialDistortParam2="-1.640373"
        stCamera:RadialDistortParam3="-9.592014">
       <stCamera:ChromaticRedGreenModel
        stCamera:ResidualMeanError="0"
        stCamera:ResidualStandardDeviation="0"
        stCamera:ScaleFactor="1.000092"
        stCamera:RadialDistortParam1="0.000833"
        stCamera:RadialDistortParam2="0.099754"
        stCamera:RadialDistortParam3="-6.555676"/>
       <stCamera:ChromaticGreenModel
        stCamera:ResidualMeanError="0"
        stCamera:ResidualStandardDeviation="0"
        stCamera:RadialDistortParam1="0.140405"
        stCamera:RadialDistortParam2="-1.640373"
        stCamera:RadialDistortParam3="-9.592014"/>
       <stCamera:ChromaticBlueGreenModel
        stCamera:ResidualMeanError="0"
        stCamera:ResidualStandardDeviation="0"
        stCamera:ScaleFactor="0.999903"
        stCamera:RadialDistortParam1="0.004197"
        stCamera:RadialDistortParam2="-0.133263"
        stCamera:RadialDistortParam3="4.477092"/>
       <stCamera:VignetteModel
        stCamera:ResidualMeanError="0.006946"
        stCamera:VignetteModelParam1="-3.975139"
        stCamera:VignetteModelParam2="-2644.757568"
        stCamera:VignetteModelParam3="81742.0625"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (w