//******************** Program Entry Point ********
:00401000 E823000000              call 00401028
:00401005 6800104000              push 00401000
:0040100A 6A00                    push 00000000

* Possible StringData Ref from Data Obj ->"Message"
                                  |
:0040100C 680C304000              push 0040300C

* Possible StringData Ref from Data Obj ->"No problem!"
                                  |
:00401011 6800304000              push 00403000
:00401016 6A00                    push 00000000

* Reference To: user32.MessageBoxA, Ord:019Dh
                                  |
:00401018 E80D000000              Call 0040102A
:0040101D 5A                      pop edx
:0040101E 81C228000000            add edx, 00000028
:00401024 FFD2                    call edx
:00401026 C3                      ret


:00401027 32                      BYTE 32h


* Referenced by a CALL at Address:
|:00401000   
|
:00401028 C3                      ret    
