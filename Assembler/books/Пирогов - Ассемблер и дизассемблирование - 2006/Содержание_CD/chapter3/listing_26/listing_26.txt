.text:00401050 sub_401050		proc near	; CODE XREF: _main+27?p
.text:00401050		var_4	= dword ptr -4
.text:00401050		arg_0	= dword ptr  8
.text:00401050		arg_4	= dword ptr  0Ch
.text:00401050		arg_8	= dword ptr  10h
.text:00401050			push    ebp
.text:00401051			mov     ebp, esp
.text:00401053			push    ecx
.text:00401054			mov     eax, [ebp+arg_0]
.text:00401057			mov     ecx, [eax]
.text:00401059			add     ecx, 0Ah
.text:0040105C			mov     edx, [ebp+arg_0]
.text:0040105F			mov     [edx], ecx
.text:00401061			mov     eax, [ebp+arg_0]
.text:00401064			mov     ecx, [eax]
.text:00401066			add     ecx, [ebp+arg_8]
.text:00401069			add     ecx, [ebp+arg_4]
.text:0040106C			mov     [ebp+var_4], ecx
.text:0040106F			mov     eax, [ebp+var_4]
.text:00401072			imul    eax, [ebp+var_4]
.text:00401076			mov     esp, ebp
.text:00401078			pop     ebp
.text:00401079			retn
.text:00401079 sub_401050	endp
