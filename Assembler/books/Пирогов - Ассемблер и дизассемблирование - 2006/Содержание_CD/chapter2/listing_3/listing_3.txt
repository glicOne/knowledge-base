* Referenced by a (U)nconditional or (C)onditional Jump at Address:
|:004420BC(C)
|
:00442123 33D2			xor edx, edx
:00442125 8B83B0010000	mov eax, dword ptr [ebx+000001B0]
:0044212B E8541DFDFF		call 00413E84
:00442130 33D2			xor edx, edx
:00442132 8B83B4010000	mov eax, dword ptr [ebx+000001B4]
:00442138 E8471DFDFF		call 00413E84
:0044213D 33D2			xor edx, edx
:0044213F 8B83B8010000	mov eax, dword ptr [ebx+000001B8]
:00442145 E83A1DFDFF		call 00413E84
:0044214A BA50000000		mov edx, 00000050
:0044214F 8B83BC010000	mov eax, dword ptr [ebx+000001BC]
:00442155 E8D618FDFF		call 00413A30

* Possible StringData Ref from Code Obj ->"Shareware Delay"
                 |
:0044215A BAA8214400		mov edx, 004421A8
:0044215F 8B83BC010000	mov eax, dword ptr [ebx+000001BC]
:00442165 E8EE1DFDFF		call 00413F58
:0044216A 33D2			xor edx, edx
:0044216C 8B83C0010000	mov eax, dword ptr [ebx+000001C0]
:00442172 E80D1DFDFF		call 00413E84
:00442177 33D2			xor edx, edx
:00442179 8B83C4010000	mov eax, dword ptr [ebx+000001C4]
:0044217F E8001DFDFF		call 00413E84
:00442184 33D2			xor edx, edx
:00442186 8B83C8010000	mov eax, dword ptr [ebx+000001C8]
:0044218C E8F31CFDFF		call 00413E84
:00442191 8B83CC010000	mov eax, dword ptr [ebx+000001CC]
:00442197 E8E8D4FFFF		call 0043F684
:0044219C 5B			pop ebx
:0044219D C3			ret 
