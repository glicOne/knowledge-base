                                  ����� 36
                   ������� � ���������� �᫠ � ����樨
                  �����������������������������������������
    ��� 㦥 �⬥砫���,  �� � ��設��� ᫮�� ��� ॣ���� ����� ������� 64�
ࠧ��� �ᥫ � ��������� �� 0000h �� FFFFh,  ��� ��  00000  ��  65535.    ��
�ࠢ������ � ⮬ ��砥,  �᫨ �� �� �������� �᫠  ��ᬠ�ਢ���,    ���
������⥫�� (����������).  �᫨,  ������,  �� �⨬ �  �����-�  �ணࠬ��
ࠡ���� ��� � ������⥫�묨, ⠪ � � ����⥫�묨 �᫠��, �.�. � �᫠��
� ������,  ��� �ਤ����  ����  �ᥫ  ��  ��  �������  ���������  (��������
����⢥��� - ��������) ����� ����⥫�묨.    �  ���᫨⥫쭮�  �孨��
�ਭ�� ����⥫�묨 ����� �� �᫠,  � ������ ��⠭����� ���訩  ���,
�.�.  �᫠ � ��������� 8000h...FFFFh.  ������⥫�묨 �� ������� �᫠  �
��襭�� ���訬 ��⮬,  �.�.  �᫠ � ���������  0000h-7FFFh.    ��  �⮬
����⥫�� �᫠ �����뢠���� � �������⥫쭮� ����, ����� ��ࠧ���� ��
��אַ�� ��⥬ ������  ���  �㫥�  �����栬�  �  �������  (�����  ���)  �
�ਡ������� � ����祭���� ��� ������� (��. 36.1).

                            ��� ����       ��� ᫮��
��אַ� ��� �᫠ 5          0000 0101       0000 0000 0000 0101
����� ��� �᫠ 5        1111 1010       1111 1111 1111 1010
                                   +1                        +1
����������������������������������������������������������������
�������⥫�� ��� �᫠ 5  1111 1011       1111 1111 1111 1011

    ���. 36.1. ��ࠧ������ ����⥫쭮�� �᫠

    ������ ����ભ���,  �� ���� �᫠ �᫮���.  ���� � � �� �᫮  FFFBh,
����ࠦ�����  �  ������  ��ப�  ��.    36.1,    �����  �  �����   ���⥪��
��ᬠ�ਢ���,  ��� ������⥫쭮� (+65531),  � � ��㣮� -  ���  ����⥫쭮�
(-5). ����� ��ࠧ��,  ���� �᫠ ���� �ࠪ���⨪�� �� ᠬ��� �᫠,  �
ᯮᮡ� ��� ��ࠡ�⪨.

    �� ��. 36.2 �।�⠢���� �롮�筠� ⠡��� 16-��⮢�� �ᥫ � 㪠������
�� �������� � ����������� ���祭��.

    ������ ����� �믮����� ����樨 �� ⮫쪮  ���  ᫮����,    ��  �  ���
���⠬�.  ��� � � ��砥 楫�� ᫮�,  �᫮ � ���� ����� ��ᬠ�ਢ���,  ���
�����������,  � ⮣�� ��� ����� �ਭ����� ���祭�� �� 000 �� 255,    ���  ���
�᫮ � ������,  � ⮣�� �������� ������⥫��� ���祭�� 㬥��蠥���  �  ���
ࠧ� (��  000  ��  127),    ��  ���������  �����������  �������  �⮫쪮  ��
����⥫��� �ᥫ (�� -001 �� -128).

    �� ��. 36.3 �।�⠢���� �롮�筠� ⠡��� ���⮢�� (8-��⮢��) �ᥫ �
㪠������ �� �������� � ����������� ���祭��.

16-�筮�      �����筮�  �।�⠢�����
�।�⠢�����  ��� �����    �� ������
0000h            00000         00000  Ŀ    ����
0001h            00001        +00001   �    �������쭮� ������⥫쭮� �᫮
0002h            00002        +00002   �
0003h            00003        +00003   �    �������� ������⥫��� �ᥫ
. . .            . . .        . . .    �
7FFEh            32766        +32766   �
7FFFh            32767        +32767  Ĵ    ���ᨬ��쭮� ������⥫쭮� �᫮
8000h            32768        -32768   �    ���ᨬ��쭮� ����⥫쭮� �᫮
8001h            32769        -32767   �
. . .            . . .        . . .    �
FFFDh            65533        -00003   �    �������� ����⥫��� �ᥫ
FFFEh            65534        -00002   �
FFFFh            65535        -00001  ��    �������쭮� ����⥫쭮� �᫮

    ���. 36.2. �।�⠢����� �������� � ����������� �ᥫ � 16-ࠧ�來��
��������
    �� ������� �  �᫠��  ᫥���  �����  �  ����  ����  ����稢����,
���஥ ����� ��⪮ ��ࠧ��� ⠪��� ᮮ⭮襭�ﬨ:

    FFFFh+0001h=0000h
    0000h-0001h=FFFFh

    �᫨ ��᫥����⥫쭮 㢥��稢��� ᮤ�ন��� ॣ���� ���  �祩��  �����,
�,  ���⨣�� ���孥�� ���������� �।��� FFFFh,  �᫮ ��३���  �१  ���
�࠭���,    �⠭��  ࠢ��  ���  �  �த�����  �������  �  ������   �����
������⥫��� �ᥫ (1,  2,  3 � �.�.).  ��筮 ⠪��,   �᫨  ��᫥����⥫쭮
㬥����� �����஥ ������⥫쭮� �᫮,  ��� ���⨣��  ���,    ��३���  �
������� ����⥫��� (���,  �� � �� ᠬ��,   ������  �����������)  �ᥫ,
��室� ���祭�� 2, 1, 0, FFFFh, FFFEh � �.�.

16-�筮�      �����筮�  �।�⠢�����
�।�⠢�����  ��� �����   �� ������
00h              000          000  Ŀ       ����
01h              001         +001   �       �������쭮� ������⥫쭮� �᫮
02h              002         +002   �
03h              003         +003   �       �������� ������⥫��� �ᥫ
. . .            . . .       . . .  �
7Eh              126         +126   �
7Fh              127         +127  Ĵ       ���ᨬ��쭮� ������⥫쭮� �᫮
80h              128         -128   �       ���ᨬ��쭮� ����⥫쭮� �᫮
81h              129         -127   �
82h              130         -126   �       �������� ����⥫��� �ᥫ
. . .            . . .       . . .  �
FEh              254         -002   �
FFh              255         -001  ��       �������쭮� ����⥫쭮� �᫮

    ��� 36.3. �।�⠢����� �������� � ����������� ���⮢�� �ᥫ

    ���, ����� ��稬, ᫥���,  �� �᫨ �� ��᫥����⥫쭮� ���騢����
�⭮�⥫쭮�� ���� � ᥣ���� ������ (�� ���筮 �ॡ����  ��  ࠡ��  �
���ᨢ���) ��३� �࠭���  ������������  �।�⠢�����  �ᥫ,    �  �����
���ᮢ����� �祩�� �� �� �।����� ��襣� ᥣ���� ������,  � �� ᠬ���  ���
��砫�.

    �।� ������ ������,  �믮������ �� ��� ���� ��ࠡ��� �ᥫ,  �����
�뤥���� �������, ������७�� � ����� �᫠ (���ਬ��,  inc,  dec,  test),
�������, �।�����祭�� ��� ��ࠡ�⪨ ����������� �ᥫ (mul, div, ja,  jb �
��.),  � ⠪�� �������,  ᯥ樠�쭮 ����⠭�� �� ��ࠡ��� �ᥫ � ������
(imul, idiv, jg, jl � �.�.).

    ���ᬮ�ਬ  �  ����⢥  �ਬ��  �������  㬭������.    ��   ���:    mul
(multiplication,  㬭������) ��� 㬭������ ����������� �ᥫ � imul  (integer
multiplication,  楫��᫥���� 㬭������) ��� ࠡ���  �  ������묨  �᫠��.
�������� �� �믮������ ��  �����  �  ��  ��  ���࠭���  �����  ࠤ����쭮
ࠧ�������.

    ��� ������� ����� ࠡ���� ��� � ᫮����, ⠪ � � ���⠬�. ��� �믮�����
㬭������ �᫠, ��室�饣��� � ॣ���� �� (� ��砥 㬭������ �� ᫮��) ���
AL (� ��砥 㬭������ �� ����),  �� ���࠭�,   �����  �����  ��室�����  �
�����-���� ॣ���� ���  �  �祩��  �����.    ��  ����᪠����  㬭������  ��
�����।�⢥���� ���祭��, � ⠪�� �� ᮤ�ন��� ᥣ���⭮�� ॣ����.

    ������ �ந��������,  �.�.  �᫮ ���⮢ � ���,  �ᥣ�� � ��� ࠧ� �����
ࠧ��� ᮬ����⥫��.   ���  �������⮢��  ����権  ����祭���  �ந��������
�����뢠���� � ॣ���� ��.  ��� ���塠�⮢��  ����権  १����  㬭������,
����� ����� ࠧ��� 32 ���,  �����뢠���� � ॣ����� DX:AX (� D�  -  �����
��������, � �� - ������).  ���ᬮ�ਬ ��᪮�쪮 �������� �ਬ�஢ ����⢨�
������ ��������� � ������������ 㬭������.

    mov AL, 3      ;���� ᮬ����⥫�=003
    mov BL, 2      ;��ன ᮬ����⥫�=002
    mul BL         ;AX=0006h=00006
    mov AL, 3      ;���� ᮬ����⥫�=003
    mov BL, 2      ;��ன ᮬ����⥫�=002
    imul BL        ;AX=0006h=+00006

    ��� �������,  mul � imul,  ���� �  ������  ��砥  ���������  १����,
��᪮��� ������� ������⥫�� �᫠ ᮢ������ �  ���������묨.    �����
�������� �� �,  �� १���� 㬭������,  ���� � ������ ��砥  ������訬,
�������� ⥬ �� ����� ���� ॣ���� ��, ����� ��� ���訩 ����.

    mov AL, 0FFh   ;���� ᮬ����⥫�=255
    mov BL, 2      ;��ன ᮬ����⥫�=002
    mul BL         ;AX=01Feh=00510
    mov AL, 0FFh   ;���� ᮬ����⥫�=-001
    mov BL, 2      ;��ன ᮬ����⥫�=002
    imul BL        ;AX=FFFEh=-00002

    ����� ����⢨� ������ mul � imul ��� ������ �  ⥬�  ��  ���࠭����  ����
ࠧ�� १�����.    �  ��ࢮ�  �ਬ��  �����������  �᫮  FFh,    ���஥
�����������,  ��� �����筮� 255,  㬭������� �� 2,  ����� �  १����
�����������  00510,    ���  0lFEh.    ��  ��஬  �ਬ��  �  ��  �᫮  FFh
��ᬠ�ਢ�����, ��� ��������.  � �⮬ ��砥 ��� ��⠢��� -001.  ���������
�� 2 ���� -002, ��� FFFEh.

    ������  ��������  ������  �  ⮣�  ��    �᫠    FFh    ���᫮�����
�ᯮ�짮������ � ��ࢮ� ��砥 ������� ��� ��ࠡ�⪨ ����������� �ᥫ,  � ��
��஬-��������. �������筠� ����� ��������� � �� 㬭������ 楫�� ᫮�:

    mov ��, 0FFFFh  ;���� ᮬ����⥫�=65535
    mov BL, 2       ;��ன ᮬ����⥫�=00002
    mul BL          ;DX:AX=0001h:FFFEh=0000131070
    mov AL, 0FFFFh  ;���� ᮬ����⥫�=00001
    mov BL, 2       ;��ன ᮬ����⥫�=00002
    imul BL         ;DX:AX=FFFFh:FFFEh=-0000000002

    � ��ࢮ� �ਬ�� �᫮ ��� ����� FFFFh (65535 � �����筮� �।�⠢�����)
㬭������� �� 2,  �����  �  १����  �᫮  ���  �����  0000131070,    ���
000lFFFEh.  �� 32- ��⮢�� �᫮ ࠧ��頥���  �  ����  ॣ�����.    �����
��������  (000lh)  �����뢠����  �  ॣ����  DX,    �����  ���   �।��饥
ᮤ�ন���,  ������ �������� (FFFEh) � ॣ���� ��.  �� ��஬ �ਬ�� �  ��
�᫮ FFFFh ��ᬠ�ਢ�����,  ���  �᫮  �  ������.    �  �⮬  ��砥  ���
��⠢��� -00001.    ���������  ��  2  ����  -0000000002,    ���  FFFFFFFEh,
��-�०���� ����� �������� �⮣� �᫠ (FFFFh) �����뢠���� � DX,   ������
�������� (FFFEh) - �  ��.

    ��㣠� ������ ��㯯� ������,  ࠧ������ �᫠ � ������ � ���  �����  -
�� ������� �᫮���� ���室��.  �� ������� ��������� �����⢫���  ���室�
��  ࠧ����  ��⪨  �ணࠬ��  �  ����ᨬ���  ��  १����    �믮������
�।��饩 �������.  ������� �᫮���� ���室�� ��� �ᯮ����� ��᫥ ������
�ࠢ����� (cmp),  ���६��� (inc),    ���६���  (dec),    ᫮�����  (add),
���⠭�� (sub), �஢�ન (test) � �鸞 ��㣨�.

    �ਢ����  ���祭�  ������  �᫮����  ���室��,      ���⢨⥫���    �
"���������" �᫠.

������� �������
      jg (jump if greater, ���室, �᫨ �����)
    jge (jump if greater or equal, ���室, �᫨ ����� ��� ࠢ��)
    jl (jump if less, ���室, �᫨ �����)
    jle (jump if less or equal, ���室, �᫨ ����� ��� ࠢ��)
    jng (jump if not greater, ���室, �᫨ �� �����)
    jnge (jump if not greater or equal, ���室, �᫨ �� ����� � �� ࠢ��)
    jnl (jump if not less, ���室, �᫨ �� �����)
    jnle (jump if not less or equal, ���室, �᫨ �� ����� � �� ࠢ��)

���������� �������
    ja (jump if above, ���室, �᫨ ���)
    jac (jump if above or equal, ���室, �᫨ ��� ��� ࠢ��)
    jb (jump if below, ���室, �᫨ ����)
    jbe (jump if below or equal, ���室, �᫨ ���� ��� ࠢ��)
    jna (jump if not above ���室, �᫨ �� ���)
    jnae (jump if not above or equal, ���室, �᫨ �� ��� � �� ࠢ��)
    jnb (jump if not below, ���室, �᫨ �� ����)
    jnbe (jump if not below or equal, ���室, �᫨ �� ���� � �� ࠢ��)

�ਬ��� ������, �����⢨⥫��� � ����� �᫠
    je (jump if equal, ���室, �᫨ ࠢ��)
    jne (jump if not equal, ���室, �᫨ �� ࠢ��)
    jc (jump if �rr�, ���室, �᫨ 䫠� CF ��⠭�����)
    jcxz (jump if CX=0, ���室, �᫨ ��=0)

    ������ �����  ������묨  �  ���������묨  ���������  �᫮����  ���室��
�����砥��� � ⮬, �� ������� ������� ��ᬠ�ਢ��� ������ "�����-�����"
�ਬ���⥫쭮 � �᫮���  ��  -32�...0...+32�,    �  ����������  �������  -
�ਬ���⥫쭮 � �᫮��� �� 0...64�.  ���⮬� ��� ������ ��������  ���室��
�᫮ 7FFFh (+32767) ����� �᫠ 8000h (-32768),  � ���  ������  �����������
7FFFh (32767) �����,  祬 8000h (32768).  �������筮 ��� ��������  ������  0
�����, 祬 FFFFh (-1), � ��� ����������� - �����.

    ����� ��ࠧ��, �� �ࠢ����� �������� �ᥫ �ᯮ������� �ନ�� "�����"
� "�����", � �� �ࠢ����� ����������� - "���" � "����".

    �� ᪠������ �ࠢ������  �  �  ⮬  ��砥,    �����  �������  �᫮����
���室�� �ᯮ������� ��� ������� ᮤ�ন���� ���⮢�� ���࠭���.  ���,  ���
�������� ������ 7Fh (+127) �����, 祬 80h (-128), � 0 �����,  祬 FFh (-1),
� ��� ����������� ������ - �������.

    ���ᬮ�ਬ ��᪮�쪮 �ਬ�஢ �ᯮ�짮����� ������ �᫮���� ���室��.

    cmp ��, limit   ;�ࠢ����� �� � ᮤ�ন���� �祩�� limit
    jb below        ;���室 �� ���� below, �᫨ AX<limit �
                    ;����������� 誠�� �ᥫ
    cmp AX, limit   ;�ࠢ����� �� � ᮤ�ন���� �祩�� limit
    jl less         ;���室.�� ���� less, �᫨ ��<limit �
                    ;�������� 誠�� �ᥫ
    cmp AL, '9'     ;�ࠢ����� ���� ASCII � AL � '9'
    ja not_num      ;���室, �᫨ �� ���
    dec SI          ;���६��� ���稪� � SI
    jl less_0       ;���室 �� less_0, �᫨ SI �⠫� ����� 0
    add ��, ��      ;�������� �� � ��
    je nul          ;���室 �� nul, �᫨ �㬬� = 0
    jge positiv     ;���室 �� positiv, �᫨ �㬬� >= 0
