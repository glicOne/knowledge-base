<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.3-c007 1.136881, 2010/06/10-18:11:35        ">
 <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about=""
    xmlns:photoshop="http://ns.adobe.com/photoshop/1.0/"
    xmlns:stCamera="http://ns.adobe.com/photoshop/1.0/camera-profile">
   <photoshop:CameraProfiles>
    <rdf:Seq>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe Systems, Inc."
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS 5D Mark II"
       stCamera:UniqueCameraModel="Canon EOS 5D Mark II"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="502"
       stCamera:Lens="EF28mm f/2.8 IS USM"
       stCamera:LensInfo="28/1 28/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS 5D Mark II"
       stCamera:LensPrettyName="Canon EF 28mm f/2.8 IS USM"
       stCamera:ProfileName="Adobe (Canon EF 28mm f/2.8 IS USM)"
       stCamera:SensorFormatFactor="0.973055"
       stCamera:ImageWidth="5634"
       stCamera:ImageLength="3753"
       stCamera:FocalLength="28"
       stCamera:FocusDistance="0.22"
       stCamera:ApertureValue="6.918863">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="0.881532"
        stCamera:FocalLengthY="0.881532"
        stCamera:ImageXCenter="0.499353"
        stCamera:ImageYCenter="0.492808"
        stCamera:ResidualMeanError="0.000129"
        stCamera:ResidualStandardDeviation="0.0002"
        stCamera:RadialDistortParam1="-0.110925"
        stCamera:RadialDistortParam2="0.109345"
        stCamera:RadialDistortParam3="-0.042659">
       <stCamera:ChromaticRedGreenModel
        stCamera:FocalLengthX="0.88155"
        stCamera:FocalLengthY="0.88155"
        stCamera:ImageXCenter="0.499364"
        stCamera:ImageYCenter="0.492808"
        stCamera:ResidualMeanError="0.000125"
        stCamera:ResidualStandardDeviation="0.000199"
        stCamera:ScaleFactor="1.000334"
        stCamera:RadialDistortParam1="-0.000611"
        stCamera:RadialDistortParam2="-0.000723"
        stCamera:RadialDistortParam3="0.000987"/>
       <stCamera:ChromaticGreenModel
        stCamera:FocalLengthX="0.88155"
        stCamera:FocalLengthY="0.88155"
        stCamera:ImageXCenter="0.499364"
        stCamera:ImageYCenter="0.492808"
        stCamera:ResidualMeanError="0.000125"
        stCamera:ResidualStandardDeviation="0.000199"
        stCamera:RadialDistortParam1="-0.110979"
        stCamera:RadialDistortParam2="0.109152"
        stCamer