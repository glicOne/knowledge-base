<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.2-c004 1.136881, 2010/06/10-18:11:35        ">
 <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about=""
    xmlns:photoshop="http://ns.adobe.com/photoshop/1.0/"
    xmlns:stCamera="http://ns.adobe.com/photoshop/1.0/camera-profile">
   <photoshop:CameraProfiles>
    <rdf:Seq>
     <rdf:li rdf:parseType="Resource">
      <stCamera:Author>Adobe Systems, Inc.</stCamera:Author>
      <stCamera:Make>Canon</stCamera:Make>
      <stCamera:Model>Canon PowerShot G10</stCamera:Model>
      <stCamera:UniqueCameraModel>Canon PowerShot G10</stCamera:UniqueCameraModel>
      <stCamera:CameraRawProfile>True</stCamera:CameraRawProfile>
      <stCamera:LensID>65535</stCamera:LensID>
      <stCamera:Lens>6.1-30.5 mm</stCamera:Lens>
      <stCamera:LensInfo>6100/1000 30500/1000 0/0 0/0</stCamera:LensInfo>
      <stCamera:CameraPrettyName>Canon PowerShot G10</stCamera:CameraPrettyName>
      <stCamera:LensPrettyName>Canon PowerShot G10</stCamera:LensPrettyName>
      <stCamera:ProfileName>Adobe (Canon PowerShot G10)</stCamera:ProfileName>
      <stCamera:SensorFormatFactor>1.684976</stCamera:SensorFormatFactor>
      <stCamera:ImageWidth>4432</stCamera:ImageWidth>
      <stCamera:ImageLength>3323</stCamera:ImageLength>
      <stCamera:FocalLength>6.1</stCamera:FocalLength>
      <stCamera:FocusDistance>0.35</stCamera:FocusDistance>
      <stCamera:ApertureValue>6</stCamera:ApertureValue>
      <stCamera:PerspectiveModel rdf:parseType="Resource">
       <stCamera:Version>2</stCamera:Version>
       <stCamera:FocalLengthX>0.827096</stCamera:FocalLengthX>
       <stCamera:FocalLengthY>0.827096</stCamera:FocalLengthY>
       <stCamera:ImageXCenter>0.5</stCamera:ImageXCenter>
       <stCamera:ImageYCenter>0.49985</stCamera:ImageYCenter>
       <stCamera:ResidualMeanError>0.000148</stCamera:ResidualMeanError>
       <stCamera:ResidualStandardDeviation>0.0001</stCamera:ResidualStandardDeviation>
       <stCamera:RadialDistortParam1>-0.157991</stCamera:RadialDistortParam1>
       <stCamera:RadialDistortParam2>0.101396</stCamera:RadialDistortParam2>
       <stCamera:ChromaticRedGreenModel rdf:parseType="Resource">
        <stCamera:FocalLengthX>0.827007</stCamera:FocalLengthX>
        <stCamera:FocalLengthY>0.827007</stCamera:FocalLengthY>
        <stCamera:ImageXCenter>0.5</stCamera:ImageXCenter>
        <stCamera:ImageYCenter>0.49985</stCamera:ImageYCenter>
        <stCamera:ResidualMeanError>0.000141</stCamera:ResidualMeanError>
        <stCamera:ResidualStandardDeviation>0.000095</stCamera:ResidualStandardDeviation>
        <stCamera:ScaleFactor>1.00085</stCamera:ScaleFactor>
        <stCamera:RadialDistortParam1>-0.001334</stCamera:RadialDistortParam1>
        <stCamera:RadialDistortParam2>0.000064</stCamera:RadialDistortParam2>
       </stCamera:ChromaticRedGreenModel>
       <stCamera:ChromaticGreenModel rdf:parseType="Resource">
        <stCamera:FocalLengthX>0.827007</stCamera:FocalLengthX>
        <stCamera:FocalLengthY>0.827007</stCamera:FocalLengthY>
        <stCamera:ImageXCenter>0.5</stCamera:ImageXCenter>
        <stCamera:ImageYCenter>0.49985</stCamera:ImageYCenter>
        <stCamera:ResidualMeanError>0.00014</stCamera:ResidualMeanError>
        <stCamera:ResidualStandardDeviation>0.000097</stCamera:ResidualStandardDeviation>
        <stCamera:RadialDistortParam1>-0.157989</stCamera:RadialDistortParam1>
        <stCamera:RadialDistortParam2>0.101409</stCamera:RadialDistortParam2>
       </stCamera:ChromaticGreenModel>
       <stCamera:ChromaticBlueGreenModel rdf:parseType="Resource">
        <stCamera:FocalLengthX>0.827007</stCamera:FocalLengthX>