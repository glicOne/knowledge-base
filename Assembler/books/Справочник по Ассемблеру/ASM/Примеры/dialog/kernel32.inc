<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.6-c011 79.156380, 2014/05/21-23:38:37        ">
 <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about=""
    xmlns:photoshop="http://ns.adobe.com/photoshop/1.0/"
    xmlns:stCamera="http://ns.adobe.com/photoshop/1.0/camera-profile">
   <photoshop:CameraProfiles>
    <rdf:Seq>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="494"
       stCamera:Lens="EF600mm f/4L IS II USM"
       stCamera:LensInfo="600/1 600/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 600mm f/4L IS II USM"
       stCamera:ProfileName="Adobe (Canon EF 600mm f/4L IS II USM)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="600"
       stCamera:FocusDistance="12.7"
       stCamera:ApertureValue="6.918863">
      <stCamera:PerspectiveModel
       stCamera:Version="2"
       stCamera:FocalLengthX="14.85663"
       stCamera:FocalLengthY="14.85663"
       stCamera:ResidualMeanError="0.000103"
       stCamera:ResidualStandardDeviation="0.000069"
       stCamera:RadialDistortParam1="0.49748"
       stCamera:RadialDistortParam2="-153.997251"
       stCamera:RadialDistortParam3="8789.14562"/>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1