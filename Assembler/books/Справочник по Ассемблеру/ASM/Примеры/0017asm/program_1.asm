include kernel32.inc 
include user32.inc 
include def32.inc 
 .386 
 .model flat 
 .data 
str1_ db 'Строка скопирована!',0 
str2_ db 21 dup(0) 
title_ db 'MessageBox',0 
library db 'DLL1.dll',0 
CopyStr db 'CopyString',0 
 .code 
_start: 
 xor ebx,ebx 
 push offset library 
 call LoadLibrary 
 push offset CopyStr 
 push eax 
 call GetProcAddress 
 push offset str1_ 
 push offset str2_ 
 call eax 
 push MB_OK 
 push offset title_ 
 push offset str2_ 
 push ebx 
 call MessageBox 
 push ebx 
 call ExitProcess 
 end _start 
