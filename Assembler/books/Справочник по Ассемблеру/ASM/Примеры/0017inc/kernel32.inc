<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.6-c011 79.156380, 2014/05/21-23:38:37        ">
 <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
  <rdf:Description rdf:about=""
    xmlns:photoshop="http://ns.adobe.com/photoshop/1.0/"
    xmlns:stCamera="http://ns.adobe.com/photoshop/1.0/camera-profile">
   <photoshop:CameraProfiles>
    <rdf:Seq>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="SONY"
       stCamera:CameraRawProfile="True"
       stCamera:PreferMetadataDistort="True"
       stCamera:Lens="FE 21mm F2.8"
       stCamera:LensInfo="210/10 210/10 28/10 28/10"
       stCamera:CameraPrettyName="Sony"
       stCamera:LensPrettyName="Sony FE 28mm F2 + Ultra Wide Converter"
       stCamera:ProfileName="Adobe (Sony FE 28mm F2 + Ultra Wide Converter)"
       stCamera:SensorFormatFactor="1"
       stCamera:FocalLength="21"
       stCamera:FocusDistance="10000"
       stCamera:ApertureValue="6">
      <stCamera:AlternateLensNames>
       <rdf:Seq>
        <rdf:li>FE 28mm F2 + Ultra Wide Converter</rdf:li>
       </rdf:Seq>
      </stCamera:AlternateLensNames>
      <stCamera:PerspectiveModel
       stCamera:Version="2"
       stCamera:ScaleFactor="1.054329"
       stCamera:RadialDistortParam1="-0.180104"
       stCamera:RadialDistortParam2="0.075739"
       stCamera:RadialDistortParam3="-0.022407"/>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="SONY"
       stCamera:CameraRawProfile="True"
       stCamera:PreferMetadataDistort="True"
       stCamera:Lens="FE 21mm F2.8"
       stCamera:LensInfo="210/10 210/10 28/10 28/10"
       stCamera:CameraPrettyName="Sony"
       stCamera:LensPrettyName="Sony FE 28mm F2 + Ultra Wide Converter"
       stCamera:ProfileName="Adobe (Sony FE 28mm F2 + Ultra Wide Converter)"
       stCamera:SensorFormatFactor="1"
       stCamera:FocalLength="21"
       stCamera:FocusDistance="3"
       stCamera:ApertureValue="8.918863">
      <stCamera:AlternateLensNames>
       <rdf:Seq>
        <rdf:li>FE 28mm F2 + Ultra Wide Converter</rdf:li>
       </rdf:Seq>
      </stCamera:AlternateLensNames>
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:ScaleFactor="1.054329"
        stCamera:RadialDistortParam1="-0.180104"
        stCamera:RadialDistortParam2="0.075739"
        stCamera:RadialDistortParam3="-0.022407">
       <stCamera:VignetteModel
        stCamera:VignetteModelParam1="-0.551693"
        stCamera:VignetteModelParam2="0.370076"
        stCamera:VignetteModelParam3="-0.196413"/>
       </rdf:Description>