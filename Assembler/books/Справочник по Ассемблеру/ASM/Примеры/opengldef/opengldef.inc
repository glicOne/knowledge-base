     stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="1.17"
       stCamera:ApertureValue="10">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:RadialDistortParam1="0.32484"
        stCamera:RadialDistortParam2="-699.215745"
        stCamera:RadialDistortParam3="-315.201928">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:VignetteModelParam1="-65.075325"
        stCamera:VignetteModelParam2="28714.451874"
        stCamera:VignetteModelParam3="-3053609.226803"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="1.17"
       stCamera:ApertureValue="10.983706">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:RadialDistortParam1="0.32484"
        stCamera:RadialDistortParam2="-699.215745"
        stCamera:RadialDistortParam3="-315.201928">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:VignetteModelParam1="-74.367692"
        stCamera:VignetteModelParam2="34394.437347"
        stCamera:VignetteModelParam3="-3935377.288732"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="1.17"
       stCamera:ApertureValue="12">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:RadialDistortParam1="0.32484"
        stCamera:RadialDistortParam2="-699.215745"
        stCamera:RadialDistortParam3="-315.201928">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:VignetteModelParam1="-93.480312"
        stCamera:VignetteModelParam2="46474.863149"
        stCamera:VignetteModelParam3="-5816191.287894"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="2.86"
       stCamera:ApertureValue="4.970854">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:RadialDistortParam1="0.32484"
        stCamera:RadialDistortParam2="-699.215745"
        stCamera:RadialDistortParam3="-315.201928">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:VignetteModelParam1="-48.52148"
        stCamera:VignetteModelParam2="-2358.344568"
        stCamera:VignetteModelParam3="-9531241.330815"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="2.86"
       stCamera:ApertureValue="6">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:RadialDistortParam1="0.32484"
        stCamera:RadialDistortParam2="-699.215745"
        stCamera:RadialDistortParam3="-315.201928">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:VignetteModelParam1="-49.004796"
        stCamera:VignetteModelParam2="45977.763716"
        stCamera:VignetteModelParam3="-15631846.784497"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="2.86"
       stCamera:ApertureValue="6.918863">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:RadialDistortParam1="0.32484"
        stCamera:RadialDistortParam2="-699.215745"
        stCamera:RadialDistortParam3="-315.201928">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:VignetteModelParam1="-32.768844"
        stCamera:VignetteModelParam2="9600.910008"
        stCamera:VignetteModelParam3="-322873.086936"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="2.86"
       stCamera:ApertureValue="8">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:RadialDistortParam1="0.32484"
        stCamera:RadialDistortParam2="-699.215745"
        stCamera:RadialDistortParam3="-315.201928">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:VignetteModelParam1="-41.908718"
        stCamera:VignetteModelParam2="14805.125356"
        stCamera:VignetteModelParam3="-981226.045057"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="2.86"
       stCamera:ApertureValue="8.918863">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:RadialDistortParam1="0.32484"
        stCamera:RadialDistortParam2="-699.215745"
        stCamera:RadialDistortParam3="-315.201928">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:VignetteModelParam1="-52.249819"
        stCamera:VignetteModelParam2="20226.693481"
        stCamera:VignetteModelParam3="-1526566.002639"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="2.86"
       stCamera:ApertureValue="10">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:RadialDistortParam1="0.32484"
        stCamera:RadialDistortParam2="-699.215745"
        stCamera:RadialDistortParam3="-315.201928">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:VignetteModelParam1="-60.706047"
        stCamera:VignetteModelParam2="25283.885539"
        stCamera:VignetteModelParam3="-2327305.713376"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="2.86"
       stCamera:ApertureValue="10.983706">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:RadialDistortParam1="0.32484"
        stCamera:RadialDistortParam2="-699.215745"
        stCamera:RadialDistortParam3="-315.201928">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:VignetteModelParam1="-67.687618"
        stCamera:VignetteModelParam2="29160.963842"
        stCamera:VignetteModelParam3="-2840312.75903"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="2.86"
       stCamera:ApertureValue="12">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:RadialDistortParam1="0.32484"
        stCamera:RadialDistortParam2="-699.215745"
        stCamera:RadialDistortParam3="-315.201928">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="11.891806"
        stCamera:FocalLengthY="11.891806"
        stCamera:VignetteModelParam1="-91.263387"
        stCamera:VignetteModelParam2="42316.281206"
        stCamera:VignetteModelParam3="-4373009.608129"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="5.06"
       stCamera:ApertureValue="4.970854">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:RadialDistortParam1="-0.199808"
        stCamera:RadialDistortParam2="-158.626273"
        stCamera:RadialDistortParam3="-11087.150612">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:VignetteModelParam1="-31.265092"
        stCamera:VignetteModelParam2="-1760.378629"
        stCamera:VignetteModelParam3="-2593294.661241"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="5.06"
       stCamera:ApertureValue="6">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:RadialDistortParam1="-0.199808"
        stCamera:RadialDistortParam2="-158.626273"
        stCamera:RadialDistortParam3="-11087.150612">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:VignetteModelParam1="-31.098394"
        stCamera:VignetteModelParam2="19797.366554"
        stCamera:VignetteModelParam3="-4552115.297866"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="5.06"
       stCamera:ApertureValue="6.918863">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:RadialDistortParam1="-0.199808"
        stCamera:RadialDistortParam2="-158.626273"
        stCamera:RadialDistortParam3="-11087.150612">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:VignetteModelParam1="-22.155106"
        stCamera:VignetteModelParam2="4657.640446"
        stCamera:VignetteModelParam3="-179703.464413"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="5.06"
       stCamera:ApertureValue="8">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:RadialDistortParam1="-0.199808"
        stCamera:RadialDistortParam2="-158.626273"
        stCamera:RadialDistortParam3="-11087.150612">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:VignetteModelParam1="-23.425052"
        stCamera:VignetteModelParam2="4873.536303"
        stCamera:VignetteModelParam3="-152803.522673"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="5.06"
       stCamera:ApertureValue="8.918863">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:RadialDistortParam1="-0.199808"
        stCamera:RadialDistortParam2="-158.626273"
        stCamera:RadialDistortParam3="-11087.150612">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:VignetteModelParam1="-32.364119"
        stCamera:VignetteModelParam2="8105.620706"
        stCamera:VignetteModelParam3="-395726.048255"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="5.06"
       stCamera:ApertureValue="10">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:RadialDistortParam1="-0.199808"
        stCamera:RadialDistortParam2="-158.626273"
        stCamera:RadialDistortParam3="-11087.150612">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:VignetteModelParam1="-37.730324"
        stCamera:VignetteModelParam2="10325.361114"
        stCamera:VignetteModelParam3="-655538.938005"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="5.06"
       stCamera:ApertureValue="10.983706">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:RadialDistortParam1="-0.199808"
        stCamera:RadialDistortParam2="-158.626273"
        stCamera:RadialDistortParam3="-11087.150612">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:VignetteModelParam1="-42.982052"
        stCamera:VignetteModelParam2="11981.129744"
        stCamera:VignetteModelParam3="-739559.159053"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="5.06"
       stCamera:ApertureValue="12">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:RadialDistortParam1="-0.199808"
        stCamera:RadialDistortParam2="-158.626273"
        stCamera:RadialDistortParam3="-11087.150612">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="9.690959"
        stCamera:FocalLengthY="9.690959"
        stCamera:VignetteModelParam1="-59.915077"
        stCamera:VignetteModelParam2="18429.085121"
        stCamera:VignetteModelParam3="-1274874.957186"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="11.9"
       stCamera:ApertureValue="4.970854">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="7.471171"
        stCamera:FocalLengthY="7.471171"
        stCamera:RadialDistortParam1="-0.270429"
        stCamera:RadialDistortParam2="-23.547518"
        stCamera:RadialDistortParam3="-3745.499961">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="7.471171"
        stCamera:FocalLengthY="7.471171"
        stCamera:VignetteModelParam1="-18.333943"
        stCamera:VignetteModelParam2="-836.602565"
        stCamera:VignetteModelParam3="-500058.348148"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="11.9"
       stCamera:ApertureValue="6">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="7.471171"
        stCamera:FocalLengthY="7.471171"
        stCamera:RadialDistortParam1="-0.270429"
        stCamera:RadialDistortParam2="-23.547518"
        stCamera:RadialDistortParam3="-3745.499961">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="7.471171"
        stCamera:FocalLengthY="7.471171"
        stCamera:VignetteModelParam1="-18.810456"
        stCamera:VignetteModelParam2="7069.052655"
        stCamera:VignetteModelParam3="-960003.984076"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="11.9"
       stCamera:ApertureValue="6.918863">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="7.471171"
        stCamera:FocalLengthY="7.471171"
        stCamera:RadialDistortParam1="-0.270429"
        stCamera:RadialDistortParam2="-23.547518"
        stCamera:RadialDistortParam3="-3745.499961">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="7.471171"
        stCamera:FocalLengthY="7.471171"
        stCamera:VignetteModelParam1="-12.073046"
        stCamera:VignetteModelParam2="1384.666707"
        stCamera:VignetteModelParam3="-23863.091568"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1Ds Mark III"
       stCamera:LensPrettyName="Canon EF 70-200mm f/2.8L IS II USM +2.0x"
       stCamera:ProfileName="Adobe (Canon EF 70-200mm f/2.8L IS II USM +2.0x)"
       stCamera:SensorFormatFactor="1"
       stCamera:ImageWidth="5640"
       stCamera:ImageLength="3752"
       stCamera:FocalLength="270"
       stCamera:FocusDistance="11.9"
       stCamera:ApertureValue="8">
      <stCamera:PerspectiveModel>
       <rdf:Description
        stCamera:Version="2"
        stCamera:FocalLengthX="7.471171"
        stCamera:FocalLengthY="7.471171"
        stCamera:RadialDistortParam1="-0.270429"
        stCamera:RadialDistortParam2="-23.547518"
        stCamera:RadialDistortParam3="-3745.499961">
       <stCamera:VignetteModel
        stCamera:FocalLengthX="7.471171"
        stCamera:FocalLengthY="7.471171"
        stCamera:VignetteModelParam1="-13.293498"
        stCamera:VignetteModelParam2="1545.965648"
        stCamera:VignetteModelParam3="-19610.285025"/>
       </rdf:Description>
      </stCamera:PerspectiveModel>
      </rdf:Description>
     </rdf:li>
     <rdf:li>
      <rdf:Description
       stCamera:Author="Adobe (www.adobe.com)"
       stCamera:Make="Canon"
       stCamera:Model="Canon EOS-1Ds Mark III"
       stCamera:UniqueCameraModel="Canon EOS-1Ds Mark III"
       stCamera:CameraRawProfile="True"
       stCamera:LensID="253"
       stCamera:Lens="EF70-200mm f/2.8L IS II USM +2.0x"
       stCamera:LensInfo="140/1 400/1 0/0 0/0"
       stCamera:CameraPrettyName="Canon EOS-1D