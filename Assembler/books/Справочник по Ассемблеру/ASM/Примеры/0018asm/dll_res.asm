include kernel32.inc
include user32.inc
include macros.inc
include def32.inc 
	.386
	.model flat
	.data
DLL	db	'dll2.dll',0
	.code
_start:
	null ebx
	run LoadLibrary, offset dll
	run DialogBoxParam, eax, 01h, ebx, offset DlgProc, ebx
	run ExitProcess, ebx

DlgProc proc
	cmp dword ptr [esp+08h],WM_CLOSE
	je close_proc
	null eax
	ret 16
close_proc:
	run EndDialog, dword ptr [esp+08h], 0
	mov eax,1
	ret 16
DlgProc endp
	end _start